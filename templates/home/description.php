<?php

    $description = get_field('description');
    $photo = $description['photo'];
    $copy = $description['copy'];

?>

<section class="description grid">

    <?php if( $photo ): ?>
        <div class="photo">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </div>
    <?php endif; ?>

    <div class="copy">
        <?php echo $copy; ?>
    </div>

</section>