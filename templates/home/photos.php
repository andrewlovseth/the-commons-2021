<?php $images = get_field('photos'); if( $images ): ?>
    <section class="photos">
        <?php foreach( $images as $image ): ?>
            <div class="photo">
                <div class="content">
                    <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>

                </div>
            </div>
        <?php endforeach; ?>
    </section>
<?php endif; ?>