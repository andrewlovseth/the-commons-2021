<?php
// Enqueue custom styles and scripts
function bearsmith_enqueue_child_styles_and_scripts() {
    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style( 'the-commons-style', $dir . '/the-commons.css', '', '' );
    wp_enqueue_script( 'the-commons-scripts', $dir . '/js/the-commons.js', '', '', true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_child_styles_and_scripts', 11);